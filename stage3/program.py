import hr
import employees
import productivity

manager = employees.Manager(1, "John Smit", 3000)
secretary = employees.Secretary(2, "Mary Poppins", 1500)
factory_worker = employees.FactoryWorker(3, "Jane Doe", 40, 25)
sales_guy = employees.SalesPerson(4, "Kevin Bacon", 1000, 500)
temporary_secretary = employees.TemporarySecretary(5, "July Roberts", 40, 15)
employees = [manager, secretary, factory_worker, sales_guy, temporary_secretary]

productivity_system = productivity.ProductivitySystem()
productivity_system.track(employees, 40)
payroll_system = hr.PayrollSystem()
payroll_system.calculate_payroll(employees)
