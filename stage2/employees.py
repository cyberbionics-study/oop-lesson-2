import hr


class Manager(hr.SalaryEmployee):
    def work(self, hours):
        print(f'{self.name} screams and yells for {hours} hours.')


class Secretary(hr.SalaryEmployee):
    def work(self, hours):
        print(f"{self.name} expends {hours} hours doing office paperwork.")


class SalesPerson(hr.CommissionEmployee):
    def work(self, hours):
        print(f"{self.name} expends {hours} hours on the phone.")


class FactoryWorker(hr.HourlyEmployee):
    def work(self, hours):
        print(f"{self.name} manufactures gadgets for {hours} hours.")


class TemporarySecretary(Secretary, hr.HourlyEmployee):
    def __init__(self, _id, name, hours_worked, hour_rate):
        hr.HourlyEmployee.__init__(self, _id, name, hours_worked, hour_rate)

    def calculate_payroll(self):
        return hr.HourlyEmployee.calculate_payroll(self)
