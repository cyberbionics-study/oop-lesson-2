import hr


salary_employee = hr.SalaryEmployee(1, "Stepan Goncharenko", 5000)
hourly_employee = hr.HourlyEmployee(2, "Dmitro Ivchenko", 40, 200)
commission_employee = hr.CommissionEmployee(3, "Oksana Fedorova", 2000, 500)

payroll_system = hr.PayrollSystem()
payroll_system.calculate_payroll(
    [salary_employee, hourly_employee, commission_employee]
)
